# Docker file developed by Ivan Hugo Guevara - PhD Student (University of Limerick)
FROM ubuntu:latest

RUN apt-get update

RUN apt-get install -y automake autoconf m4 git make clang

# These commands copy your files into the specified directory in the image
COPY . /home/BDS/ga
WORKDIR /home/BDS/ga

# This command compiles your app using GCC, adjust for your source code
RUN make
RUN make install
RUN cd /home/BDS/ga/examples && make

# This command runs your application, comment out this line to compile only

LABEL Name=ga-docker Version=0.0.1
